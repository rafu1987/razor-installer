<?php

/***************************************************************
 * 
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Raphael Zschorsch, http://www.rafu1987.de
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

// Include class
require_once 'App/Installer.class.php';

// Init class
$installer = new Installer();

// Ajax
if(isset($_POST['install']) && $_POST['install'] == true) {
  $config = $installer->getConfigFromPost($_POST);

  $installer->init($config);

  return;
}

$versions = $installer->getVersions();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>razor Installer - powered by @rafu1987</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="App/css/normalize.min.css">
  <link rel="stylesheet" href="App/css/skeleton.min.css">
  <link rel="stylesheet" href="App/css/App.min.css">
  <link rel="icon" type="image/png" href="App/images/favicon.png">
</head>
<body>
  <div class="fake-progressbar"></div>
  <div class="container">
    <div class="row">
      <div class="one-half column" style="margin-top: 20%">
        <h3><i class="razor-typo3"></i> razor Installer</h3>
        This is the official <b>razor</b> Installer. Just click the button below to kickstart a new TYPO3 project with razor.<br />
        <span class="small">powered by <a href="https://github.com/rafu1987" target="_blank">@rafu1987</a></span></p>

        <form>
        <div class="install-options">
          <label for="typo3version">TYPO3 version</label>
          <select class="u-full-width" id="typo3version" name="version">
            <?php

              $options = '';
              foreach($versions as $versionKey => $versionVal) {
                if($versionVal['info']) {
                  $info = ' ' . $versionVal['info'];
                }

                $options .= '<optgroup label="'.$versionKey . $info.'">';
                unset($versionVal['info']);
                
                foreach($versionVal as $vKey => $vVal) {
                  $options .= '<option value="'.$vKey.'">'.$vVal.'</option>';
                }

                $options .= '</optgroup>';
              }

              echo $options;

            ?>
          </select>

          <label for="typo3version"><a href="javascript:void(0);" class="more-options">More options <i class="razor-angle-right"></i></a></label>
          <div class="options">
            <div class="options-inner">
              <div class="row">
                <div class="six columns">
                  <label for="typo3srcfolderpath">Path to "typo3_src" folder</label>
                  <input class="u-full-width" type="text" placeholder="../" id="typo3srcfolderpath" name="folderPath">
                </div>
                <div class="six columns">
                  <label for="typo3srcfoldername">"typo3_src" folder name</label>
                  <input class="u-full-width" type="text" placeholder="typo3_src" id="typo3srcfoldername" name="folderName">
                </div>
              </div>
              <div class="row">
                <div class="twelve columns">
                  <label for="removePrefix">Remove "typo3_src" prefix on source folder</label>
                  <input type="checkbox" name="prefix" id="prefix" />
                </div>
              </div>
            </div>
          </div>

       	  <button type="submit" class="button-start button-primary button-big button-orange" name="install">Install <i class="razor-typo3"></i></button>
        </div>
        </form>
        
        <div class="success-message">
          <p>TYPO3 with razor has been kickstarted. This page will be reloaded automatically.</p>
        </div>
      </div>
    </div>
  </div>
  <script src="App/js/jquery-1.11.2.min.js"></script>
  <script src="App/js/pace.min.js" data-pace-options='{ "startOnPageLoad": false }'></script>
  <script src="App/js/App.min.js"></script>
</body>
</html>