<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * 
 * @package default
 */
class Installer
{
  
  private $config = array(
    'latestSource' => 'http://get.typo3.org/', 
    'razor' => 'https://bitbucket.org/rafu1987/razor/get/master.zip', 
    'razorZip' => 'razor.zip', 
    'branches' => '6.2,7', 
    'notRecommended' => '7', 
    'excludeStates' => 'alpha,beta,rc,snapshot', 
    'notRecommendedLabel' => '(Not recommended)', 
    'latestLabel' => 'Latest', 
    'versionsFileName' => 'versions.conf', 
    'versionsJson' => 'http://get.typo3.org/json'
  );
  
  private $options = array();
  
  /**
   * Constructor
   * @return type
   */
  public function __construct()
  {
    $this->config['projectFolder'] = realpath(dirname(__FILE__)) . '/../';
  }
  
  /**
   * Initialization
   * @param string $config 
   * @return type
   */
  public function init($config)
  {
    // Set options
    foreach ($config as $optionKey => $optionVal) {
      $this->options[$optionKey] = $optionVal;
    }
    
    // Create typo3_src dir if not there
    $folderPath = $this->options['folderPath'];
    if (!is_dir($folderPath)) {
      mkdir($folderPath);
    }
    
    // Kick-off
    $this->start();
  }
  
  /**
   * Start function
   * @return type
   */
  private function start()
  {
    // Download latest source if it doesn't exist
    if (!is_dir($this->options['folderPath'] . $this->options['prefix'] . $this->options['version'])) {
      // Get typo3_src
      $this->getSource();
    } else {
      // Create typo3conf
      $this->createTYPO3Conf();
      
      // Create symlinks
      $this->createSymlinks();
      
      // Create FIRST_INSTALL
      $this->createFile('FIRST_INSTALL');
      
      // Create index.php
      $this->createIndex();
      
      // Get razor distribution
      $this->getRazor();
      
      // Rename index files
      $this->finish();
    }
  }
  
  /**
   * Get TYPO3 source files
   * @return type
   */
  private function getSource()
  {
    $version      = $this->options['version'];
    $prefix       = $this->options['prefix'];
    $latestSource = $this->config['latestSource'];
    
    // Set typo3_src
    $latestSource .= $version . '/zip';
    $source   = $latestSource;
    $filename = $prefix . $version . '.zip';
    
    // Download typo3_src
    if (!is_file($this->options['folderPath'] . $filename)) {
      $this->getZip($source, $this->options['folderPath'] . $filename);
    }
    
    // Unzip typo3_src
    $this->unzip($this->options['folderPath'] . $filename, $prefix . $version, '', $this->options['folderPath'], $this->options['folderPath'], 'typo3_src-' . $version, $this->options['renameSrc'], 1, 'src');
    
    // Start again
    $this->start();
    
    return false;
  }
  
  /**
   * Get razor extension
   * @return type
   */
  private function getRazor()
  {
    $razorZip      = $this->config['razorZip'];
    $razor         = $this->config['razor'];
    $projectFolder = $this->config['projectFolder'];
    
    if (!is_file($razorZip)) {
      $this->getZip($razor, $razorZip);
    }
    
    // Unzip razor
    $this->unzip($razorZip, 'razor', $projectFolder, 'typo3conf/ext/', '', 'unknown', 1, 1, 'ext');
  }
  
  /**
   * Finish
   * @return type
   */
  private function finish()
  {
    $projectFolder = $this->config['projectFolder'];
    
    rename($projectFolder . 'index.php', $projectFolder . 'index_old.php');
    rename($projectFolder . 'index_new.php', $projectFolder . 'index.php');
    
    // Delete versions file
    unlink($this->config['versionsFileName']);
  }
  
  /**
   * Create typo3conf related stuff
   * @return type
   */
  private function createTYPO3Conf()
  {
    $projectFolder = $this->config['projectFolder'];
    
    mkdir($projectFolder . 'typo3conf');
    mkdir($projectFolder . 'typo3conf/ext');
    
    $this->createFile('typo3conf/ENABLE_INSTALL_TOOL');
  }
  
  /**
   * Create symlinks
   * @return type
   */
  private function createSymlinks()
  {
    $typo3_src              = $this->options['folderName'];
    $typo3_src_symlink_name = 'typo3_src';
    $typo3                  = 'typo3';
    
    // typo3_src
    if (!is_link($typo3_src)) {
      symlink($this->options['folderPath'] . $this->options['prefix'] . $this->options['version'], $typo3_src_symlink_name);
    }
    
    // typo3
    if (!is_link($typo3)) {
      symlink('typo3_src/typo3', $typo3);
    }
  }
  
  /**
   * Create index file
   * @return type
   */
  private function createIndex()
  {
    $typo3_src_symlink_name = 'typo3_src';
    
    $myfile = fopen("index_new.php", "w") or die("Unable to open file!");
    $txt = '<?php
            include_once("' . $typo3_src_symlink_name . '/index.php");
        ?>';
    fwrite($myfile, $txt);
    fclose($myfile);
  }
  
  /**
   * Download a zip file
   * @param string $url 
   * @param string $zipFile 
   * @return type
   */
  private function getZip($url, $zipFile)
  {
    $zipResource = fopen($zipFile, "w");
    
    // Get The Zip File From Server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FILE, $zipResource);
    $page = curl_exec($ch);
    
    if (!$page) {
      echo "Error :- " . curl_error($ch);
    }
    
    curl_close($ch);
  }
  
  /**
   * Unzip a file
   * @param string $zipFile 
   * @param string $folderName 
   * @param string $projectFolder 
   * @param string $extractPath 
   * @param string $downloadPath 
   * @param string $oldFolderName 
   * @param boolean $rename 
   * @param boolean $delete 
   * @param boolean $type 
   * @return type
   */
  private function unzip($zipFile, $folderName, $projectFolder = '', $extractPath, $downloadPath = '', $oldFolderName = '', $rename = 0, $delete = 0, $type)
  {
    // Open the Zip file
    $zip = new ZipArchive;
    if ($zip->open($zipFile) != "true") {
      echo "Error :- Unable to open the Zip File";
    }
    
    // Extract Zip File
    $zip->extractTo($extractPath);
    $zip->close();
    
    // Rename unzipped folder
    if ($rename == 1) {
      if ($oldFolderName == 'unknown') {
        $dirs = array_filter(glob($extractPath . '*'), 'is_dir');
        foreach ($dirs as $dir) {
          rename($dir, $projectFolder . $extractPath . $folderName);
        }
      } else {
        rename($projectFolder . $extractPath . $oldFolderName, $projectFolder . $extractPath . $folderName);
      }
    }
    
    // Delete zip file
    if ($delete == 1) {
      if (is_dir($projectFolder . $extractPath . $folderName)) {
        if ($type == 'ext') {
          unlink($projectFolder . $downloadPath . $zipFile);
        } else if ($type == 'src') {
          unlink($projectFolder . $zipFile);
        }
      }
    }
  }
  
  /**
   * Get TYPO3 versions
   * @param string $url 
   * @return type
   */
  public function getVersions()
  {
    // Branches
    $branches       = explode(",", $this->config['branches']);
    $notRecommended = explode(",", $this->config['notRecommended']);
    $excludeStates  = explode(",", $this->config['excludeStates']);
    
    // Check if there is a cache file already
    $filename = $this->config['versionsFileName'];
    
    if (is_file($filename)) {
      $versions = $this->readVersionsCacheFile($filename);
      
      // If file is empty for some reason
      if (!$versions) {
        // Remove file
        unlink($filename);
        
        // Get versions again
        return $this->getVersions();
      }
    } else {
      // Get json feed
      $url  = $this->config['versionsJson'];
      $feed = $this->file_get_contents_curl($url);
      $json = json_decode($feed, true);
      
      $versions = array();
      $i        = 0;
      foreach ($branches as $branch) {
        $singleVersion = array();
        foreach ($json[$branch]['releases'] as $release) {
          $version = $release['version'];
          
          if ($this->strposa($version, $excludeStates)) {
            if ($i == 0) {
              $singleVersion[$version] = $this->config['latestLabel'] . ' (' . $version . ')';
            } else {
              $singleVersion[$version] = $version;
            }
          }
          
          $i++;
        }
        
        // Add .x to branch
        $versions[$branch . '.x'] = $singleVersion;
        
        // Not recommended
        if (in_array($branch, $notRecommended)) {
          $versions[$branch . '.x']['info'] = $this->config['notRecommendedLabel'];
        }
      }
      
      // Save versions to text file (caching)
      $this->versionsCache($versions, $filename);
    }
    
    return $versions;
  }
  
  /**
   * Get config from $_POST
   * @param string $config 
   * @return type
   */
  public function getConfigFromPost($config)
  {
    $newConfig = array();
    foreach ($config['formData'] as $conf) {
      // Folder path
      if ($conf['name'] == 'folderPath') {
        if (!$conf['value'])
          $conf['value'] = '../';
      }
      
      // Folder name
      if ($conf['name'] == 'folderName') {
        if (!$conf['value'])
          $conf['value'] = 'typo3_src';
      }
      
      // Prefix
      if ($conf['name'] == 'prefix') {
        $conf['value'] = '';
      }
      
      $newConfig[$conf['name']] = $conf['value'];
    }
    
    // Add folder name to folder path
    $newConfig['folderPath'] .= $newConfig['folderName'] . '/';
    
    // Add removePrefix
    if (!isset($newConfig['prefix'])) {
      $newConfig['prefix']    = 'typo3_src-';
      $newConfig['renameSrc'] = 0;
    } else {
      $newConfig['renameSrc'] = 1;
    }
    
    return $newConfig;
  }
  
  /**
   * curl file_get_contents alternative
   * @param string $url 
   * @return type
   */
  private function file_get_contents_curl($url)
  {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_URL, $url);
    
    $data = curl_exec($ch);
    curl_close($ch);
    
    return $data;
  }
  
  /**
   * Create empty file
   * @param string $path 
   * @return type
   */
  private function createFile($path)
  {
    fopen($path, "w");
  }
  
  /**
   * Create TYPO3 versions cache file
   * @param string $versions 
   * @param string $filename 
   * @return type
   */
  private function versionsCache($versions, $filename)
  {
    $fn = $filename;
    $fh = fopen($fn, 'w');
    fwrite($fh, serialize($versions));
    fclose($fh);
  }
  
  /**
   * Read TYPO3 versions cache file
   * @param string $filename 
   * @return type
   */
  private function readVersionsCacheFile($filename)
  {
    $file = fopen($filename, "r") or die("Unable to open file!");
    
    // If file is not empty
    if (filesize($filename) != 0) {
      $versions = fread($file, filesize($filename));
      $versions = unserialize($versions);
      fclose($file);
      
      return $versions;
    }
  }
  
  /**
   * strposa
   * @param string $haystack 
   * @param string $needle 
   * @param boolean $offset 
   * @return type
   */
  private function strposa($haystack, $needle, $offset = 0)
  {
    if (!is_array($needle))
      $needle = array(
        $needle
      );
    foreach ($needle as $query) {
      if (strpos($haystack, $query, $offset) !== false)
        return false;
    }
    return true;
  }
  
  /**
   * Debug
   * @param string $var 
   * @return type
   */
  private function debug($var)
  {
    print_r("<pre>") . print_r($var) . print_r("</pre>");
  }
  
  /**
   * Log
   * @param string $var 
   * @param string $filename 
   * @param boolean $append 
   * @return type
   */
  private function log($var, $filename = 'log.txt', $append = 1)
  {
    $projectFolder = $this->config['projectFolder'];
    
    ob_start();
    var_dump($var);
    
    if ($append == 1) {
      file_put_contents($projectFolder . $filename, ob_get_clean(), FILE_APPEND);
    } else {
      file_put_contents($projectFolder . $filename, ob_get_clean());
    }
  }
}